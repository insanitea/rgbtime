#include <wat/colour.h>
#include <wat/range.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

bool use_hsv = false;
double base_v = 0x00;

int main (int argc, char **argv) {
  int i,z;
  time_t now;
  struct tm t;
  int values[3],
      ranges[3];
  struct rgba rgb_c;
  struct hsva hsv_c;
  double *rgb_v,
         *hsv_v;

  for(i=1, z=argc-1; i < argc; ++i) {
    if(strcmp(argv[i], "-hsv") == 0)
      use_hsv = true;
    else if (strcmp(argv[i], "-base") == 0 && i < z)
      base_v = strtoul(argv[++i], NULL, 0);
  }

  time(&now);
  localtime_r(&now, &t);

  values[0] = t.tm_hour;
  values[1] = t.tm_min;
  values[2] = t.tm_sec;

  ranges[0] = 23;
  ranges[1] = 59;
  ranges[2] = 59;

  if(use_hsv) {
    rgb_v = &rgb_c.r;
    hsv_v = &hsv_c.h;

    //  (HOUR[0-23],MIN[0-59],SEC[0-59]) -> HSV[0-1]
    for(i=0; i < 3; ++i)
      hsv_v[i] = convert_range(values[i], 0,ranges[i], 0.0,1.0);

    //  HSV[0-1] -> RGB[0-1]
    hsva_to_rgba(&hsv_c, &rgb_c);

    //  RGB[0-1] -> RGB[BASE-255]
    for(i=0; i < 3; ++i)
      values[i] = convert_range(rgb_v[i], 0.0,1.0, base_v,UINT8_MAX);
  }
  else {
    //  (HOUR[23], MIN[59], SEC[59]) -> RGB[BASE-255]
    for(i=0; i < 3; ++i)
      values[i] = convert_range(values[i], 0,ranges[i], base_v,UINT8_MAX);
  }

  printf("#%02X%02X%02X", values[0], values[1], values[2]);
  return 0;
}
